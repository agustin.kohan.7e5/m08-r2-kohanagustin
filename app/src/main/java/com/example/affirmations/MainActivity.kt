/*
 * Copyright (C) 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.affirmations

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.StringRes
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material.icons.filled.ExpandLess
import androidx.compose.material.icons.filled.ExpandMore
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.affirmations.data.Datasource
import com.example.affirmations.model.Affirmation
import com.example.affirmations.ui.theme.AffirmationsTheme
import com.example.affirmations.ui.theme.Typography


class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            // TODO 5. Show screen
            AffirmationApp()
        }
    }
}

@Composable
fun AffirmationApp() {
    // TODO 4. Apply Theme and affirmation list
    AffirmationsTheme() {
        AffirmationList(affirmationList = Datasource().loadAffirmations())
    }
}

@Composable
fun AffirmationList(affirmationList: List<Affirmation>, modifier: Modifier = Modifier) {
    // TODO 3. Wrap affirmation card in a lazy column
    LazyColumn(modifier = modifier) {
        items(affirmationList) { affirmation ->
            AffirmationCard(
                affirmation = affirmation, modifier = Modifier
                    .padding(8.dp)
                    .fillMaxSize()
            )
        }
    }

}

@Composable
fun OnButton(
    expanded: Boolean, onClick: () -> Unit, modifier: Modifier = Modifier

) {
    IconButton(onClick = onClick) {
        Icon(
            imageVector = if (expanded) Icons.Filled.ExpandLess else Icons.Filled.ExpandMore,
            tint = MaterialTheme.colors.primary,
            contentDescription = "Expand Button"/*stringResource(R.string.expand_button_content_description)*/
        )
    }
}

@Composable
fun CardInfo(modifier: Modifier = Modifier, description: String) {
    Row(horizontalArrangement = Arrangement.Center) {
        Column(
            modifier = modifier.padding(
                start = 16.dp, top = 8.dp, bottom = 16.dp, end = 16.dp
            )
        ) {
            Text(
                text = "Descripción",
                fontSize = 24.sp,
                fontWeight = FontWeight(1000)
            )
            Text(
                text = description,
                fontSize = 14.sp
            )
        }

        Spacer(modifier = Modifier.weight(0.5f))
        Button(
            onClick = { /*TODO*/ }, modifier = Modifier.padding(8.dp)
        ) {
            Text(text = "Mondongo")
        }
    }

}

@Composable
fun AffirmationCard(affirmation: Affirmation, modifier: Modifier = Modifier) {
    var expanded by remember { mutableStateOf(false) }

    val color by animateColorAsState(
        targetValue = if (expanded) Color.Cyan else MaterialTheme.colors.surface,
    )
    // TODO 1. Your card UI
    Card(
        modifier = modifier,
        elevation = 15.dp
    ) {

        Column(
            modifier = Modifier
                .background(color = color)
                .animateContentSize(
                    animationSpec = spring(
                        dampingRatio = Spring.DampingRatioMediumBouncy,
                        stiffness = Spring.StiffnessLow
                    )
                )
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                Image(
                    modifier = Modifier
                        .padding(8.dp)
                        .size(64.dp),
                    painter = painterResource(id = affirmation.imageResourceId),
                    contentDescription = stringResource(id = affirmation.stringResourceId),
                    contentScale = ContentScale.Crop
                )
                Text(
                    text = stringResource(id = affirmation.stringResourceId),
                    modifier = Modifier
                        .padding(8.dp)
                        .weight(1f),
                    style = Typography.body1
                )
                OnButton(
                    expanded = expanded,
                    onClick = { expanded = !expanded },
                    modifier = modifier
                )
            }
            if (expanded) {
                CardInfo(modifier = modifier, stringResource(affirmation.descriptionResourceId))
            }
        }
    }

}

@Preview
@Composable
private fun AffirmationCardPreview() {
    // TODO 2. Preview your card
    AffirmationCard(
        affirmation = Affirmation(
            R.string.affirmation1,
            R.drawable.image1,
            R.string.description1
        )
    )
}
